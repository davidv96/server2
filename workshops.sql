DROP DATABASE IF EXISTS server2;
CREATE DATABASE  server2;
\c server2;



--
-- Table structure for table of enrolled students
--

DROP TABLE IF EXISTS workshops;
CREATE TABLE workshops (
  id serial NOT NULL,
  date,
  location varchar (35),
  maxseats int NOT NULL default 5,
  instructor varchar(35) NOT NULL default ''
);


DROP TABLE IF EXISTS users;
CREATE TABLE users (
	id serial primary key,
	firstname varchar (35),
	lastname varchar (35),
	username varchar (35),
	email varchar (35)
);


DROP TABLE IF EXISTS enrolled;
CREATE TABLE enrolled (
	workshop_id int,
	user_id
	);



DROP Role if EXISTS swift;
create user swift with password 'RunningServer12!';
Grant select, insert, delete on workshops, users, enrolled to swift;
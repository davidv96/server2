const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');


const app = express();
var config = {
host: 'localhost',
      user: 'classmanager',
      password: 'RunningServer12!',
      database: 'server2',
};

var pool = new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));



//GET REQUEST


app.get('/list-users', async (req, res) => {

		var type = req.query.type;

		//if no type given
		if(!type || type===""){


		//

		res.json({error: 'Would you like a summary or full description of users?'});

		}

		else{


		try{

		//get list of workshops
		var userQuery = await pool.query('select * from users;')

			//put in nice JSON list
			let allUsers = {users: []};
		     allUsers.users = userQuery.rows.map(function(item){
				let firstname = item.firstname;
				let lastname = item.lastname;
				let username = item.username;
				let email = item.email;
				if(type === "summary"){
				return {
firstname: firstname,
lastname: lastname
};
}
if(type === "full"){
return {
firstname: firstname,
lastname: lastname,
username: username,
email: email
};
}
});


res.json({allUsers});






}
catch(e) {
	console.error('Error running query ' + e);
}

}
});


app.get('/attendees', async (req, res) => {

		var title = req.query.title;
		var classdate = req.query.date;
		var location = req.query.location;

		//if missing variables given
		if(!title || !classdate || ! location){


		//
		console.log(JSON.stringify(title));
		res.json({error: 'Missing one or more variables'});

		}
		else{


		try{

			var enrolledUsers = await pool.query('select users.firstname, users.lastname from users inner join enrolled on users.id = enrolled.user_id and enrolled.workshop_id = (select id from workshops where title = $1 AND classdate = $2 AND location = $3);',
				[title, classdate, location]);

			//put in nice JSON list
			let allUsers = {users: []};
			allUsers.users = enrolledUsers.rows.map(function(item){
					let firstname = item.firstname;
					let lastname = item.lastname;
					return {
						firstname: firstname,
						lastname: lastname
						};
						});


res.json({allUsers});

}
catch(e) {
	console.error('Error running query ' + e);
}

}
});











app.get('/list-workshops', async (req, res) => {



try{

		//get list of workshops
		var workshopQuery = await pool.query('select * from workshops;')

			//put in nice JSON list
			let allWorkshops = {workshop: []};
		     allWorkshops.workshops = workshopQuery.rows.map(function(item){
				let title = item.title;
				let date = item.classdate;
				let location = item.location;
				let maxseats = item.maxseats;
				let instructor = item.instructor;
				return {
				title: title,
				date: date,
				location: location,
				maxseats: maxseats,
				instructor: instructor
				};
				});


res.json({allWorkshops});


}
catch(e) {
	console.error('Error running query ' + e);
}

});




//CREATE USER

app.post('/create-user', async (req, res) => {
		console.log(req.body);
		var firstname = req.body.firstname;
		var lastname = req.body.lastname;
		var username = req.body.username;
		var email = req.body.email;


		//check that all params given
		if(!firstname || !lastname || !username || !email){
		res.json({error: 'One or more parameters not given'});
		}

		else{
		try{

		//check if user already exists
		var userCheck = await pool.query('select * from users where username = $1;',
				[username]);


		//if count is not zero, there should be someone in that class, already enrolled
		if(userCheck.rowCount != 0){
			res.json({status: 'Username already exists. Create unique username.'});
		}

		else{
			await pool.query('insert into users(firstname, lastname, username, email) values($1, $2,$3,$4);',
					[firstname,lastname, username, email]);
			res.json({status: 'User added.'});
		}



		}
		catch(e) {
			console.error('Error creating user: ' + e);
		}

		}




});







//SEND/POST REQUEST

app.post('/add-workshop', async (req, res) => {
		console.log(req.body);
		var title = req.body.title;
		var classdate = req.body.date;
		var location = req.body.location;
		var maxseats = req.body.maxseats;
		var instructor = req.body.instructor;


		//if the fields not given
		if(!title || !classdate || !location || !maxseats || !instructor){
		res.json({error: 'Some parameter not given. Date must be in YYYY-MM-DD format!'});
		}

		//if fields given, begin trying to post
		else{
		try{



		var workshopCheck = await pool.query('select * from workshops where title = $1 AND classdate = $2 AND location = $3;',
				[title, classdate, location]);



		//if count is not zero, there should be someone in that class, already enrolled
		if(workshopCheck.rowCount != 0){
			res.json({status: 'Workshop already exists.'});
		}else{

			await pool.query('insert into workshops(title, classdate, location, maxseats, instructor) values ($1, $2, $3, $4, $5)',
					[title, classdate, location, maxseats, instructor]);
			res.json({status: 'Workshop created.'});

		}

		}
		catch(e) {
			console.error('Error posting' + e);
		}
		}

});



//enroll student 

app.post('/enroll', async(req, res) => {
    var username = req.body.username;
    var title = req.body.title;
    var classdate = req.body.date;
    var location = req.body.location;

    if(!title || !username || !location || !classdate){
        res.json({error: "Parameters not given"});
    }
    else{
        try{
			var workshopCheck = await pool.query('select * from workshops where title = $1 AND classdate = $2 AND location = $3;',
				[title, classdate, location]);
            

            var userCheck = await pool.query('select * from users where username = $1;',
				[username]);

        if(workshopCheck.rows.length === 0){
            res.json({status: "workshop does not exist"});
            console.log("wscheck");
        }


        if(userCheck.rows.length === 0){
            res.json({status: "user does not exist"});
            console.log("usercheck");
        }


 

        var seatsTaken = await pool.query('select count (title) from workshops inner join enrolled on workshops.id = enrolled.workshop_id where workshops.title = $1 and workshops.classdate = $2 and workshops.location = $3;',
        	[title,classdate,location]);
        console.log(seatsTaken.rows[0]);

        var shopSeats = await pool.query('select maxseats from workshops where title = $1 AND classdate = $2 AND location = $3;',
				[title, classdate, location]);
        console.log(shopSeats.rows[0]);

        if(seatsTaken.rows[0].count >= shopSeats.rows[0].maxseats){
        	res.json({error: "There are no more seats in this workshop."});
        	return;
        }



        var checkRepeat = await pool.query('select count (user_id) from enrolled where user_id = (select id from users where username = $1) and workshop_id = (select id from workshops where title = $2 AND classdate = $3 AND location = $4);',
				[username, title, classdate, location]);

        if(checkRepeat.rows[0].count != 0){
        	 res.json({error: "User already enrolled."});
            console.log("repeatcheck");

        }
        

        else{


            await pool.query('insert into enrolled (user_id, workshop_id) values ( (select id from users where username = $1), (select id from workshops where title = $2 AND classdate = $3 AND location = $4) );',
				[username, title, classdate, location]);
                res.json({status: "user added"});
            }
            
        }
        catch(e){
                console.log("Error running insert " + e);
            } 

    }

});




//DELETE REQUEST

app.delete('/delete-user', async (req, res) => {
		console.log(req.body);
		var username = req.body.username;
		if(!username){
		res.json({error: 'Username not given'});
		}
		else{
		try{

		var response = await pool.query('delete from users where username = $1',
				[username]);
		res.json({status: 'deleted'});
		}
		catch(e) {
		console.error('Error deleting' + e);
		}
		}

		});




app.listen(app.get('port'), () => {
		console.log('Running');
		})

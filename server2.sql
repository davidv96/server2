DROP DATABASE IF EXISTS server2;
CREATE DATABASE  server2;
\c server2;



--
-- Table structure for table of enrolled students
--

DROP TABLE IF EXISTS workshops;
CREATE TABLE workshops (
  id serial NOT NULL,
  title varchar (35),
  classdate date,
  location varchar (35),
  maxseats int NOT NULL default '5',
  instructor varchar(35) NOT NULL default '',
  PRIMARY KEY(id)
);


DROP TABLE IF EXISTS users;
CREATE TABLE users (
	id serial NOT NULL,
	firstname varchar (35) default '',
	lastname varchar (35) default '',
	username varchar (35) default '',
	email varchar (35) default '',
	PRIMARY KEY(id)
);


DROP TABLE IF EXISTS enrolled;
CREATE TABLE enrolled (
	workshop_id int references workshops(id),
	user_id int references users(id),
	PRIMARY KEY(workshop_id, user_id)
);






--workshops
insert into workshops (title, classdate, location, maxseats, instructor) values ('React',
'2017-08-29', 'Hurley CC', '5', 'Zacharski');

insert into workshops (title, classdate, location, maxseats, instructor) values ('Java',
'2017-08-29', 'Trinkle', '5', 'Cooper');

insert into workshops (title, classdate, location, maxseats, instructor) values ('Python',
'2017-08-29', 'Monroe', '5', 'Finlayson');



--users
insert into users (firstname, lastname, username, email) values ('Joe', 'Smith', 'jsmith',
'jsmith@mail.com');

insert into users (firstname, lastname, username, email) values ('Carl', 'Jones', 'cjones',
'cjones@mail.com');

insert into users (firstname, lastname, username, email) values ('Rose', 'Black', 'rblack',
'rblack@mail.com');



--enrolled
insert into enrolled(workshop_id, user_id) values ((select id from workshops where title = 'React'),
(select id from users where username = 'jsmith'));

insert into enrolled(workshop_id, user_id) values ((select id from workshops where title = 'Java'),
(select id from users where username = 'cjones'));


DROP Role if EXISTS classmanager;
create user classmanager with password 'RunningServer12!';
Grant all on workshops, users, enrolled to classmanager;
grant usage on users_id_seq to classmanager;
grant usage on workshops_id_seq to classmanager;